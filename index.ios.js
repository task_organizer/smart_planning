/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry
} from 'react-native';

import Application from './src/index';

import {Container, Content} from 'native-base';

export default class smart_planning extends Component {
    render() {

        return (
            <Container>
                <Application/>
            </Container>
        );
    }
}

AppRegistry.registerComponent('smart_planning', () => smart_planning);
