import * as _ from 'lodash';

export function isDayPlanEventDone(event) {

    const taskStatus = _.get(event, 'task.status');

    return taskStatus === 'done';

}

export function isTodoDone(todo) {

    const status = _.get(todo, 'status');

    return status === 'done';

}