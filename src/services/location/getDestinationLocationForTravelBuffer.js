import * as _ from 'lodash';

export default function getDestinationLocationAndTravelModeForTravelBuffer(travelBuffer) {

    const travelMode = travelBuffer.travelMode;
    const directions = travelBuffer.directions;

    if (travelMode && directions) {

        const directionsForTravelMode = directions[travelMode];

        const request = _.get(directionsForTravelMode, 'route.request');

        if (request) {
            const travelMode = request.travelMode;
            const destination = _.get(request, 'destination.location');

            return {
                travelMode,
                lat: destination.lat,
                lng: destination.lng
            }

        }


    }

    throw new Error('travelBuffer is corrupted, not possible to retrieve travel mode and destination');

}