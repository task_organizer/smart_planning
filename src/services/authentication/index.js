import {AsyncStorage}from 'react-native';
import * as moment from 'moment';
import * as _ from 'lodash';
import {
    decodeToken
} from 'jwt-node-decoder';

export async function isAuthenticated() {

    const userDataJson = await AsyncStorage.getItem('userData');

    if(!userDataJson) {
        return false;
    }

    const userData = JSON.parse(userDataJson);

    if (!userData || !userData.token) {
        return false;
    }

    const token = await decodeToken(userData.token.token);
    const isTokenExpired = moment.unix(token.exp) < moment.now();

    return !isTokenExpired;

}

export async function getUserData() {

    const userData = await AsyncStorage.getItem('userData');

    if(_.isString(userData)) {

        return JSON.parse(userData);

    }

    return null;

}

export async function saveUserData(userData) {

    const userDataJson = JSON.stringify(userData);
    await AsyncStorage.setItem('userData', userDataJson);

}

export async function cleanUserData() {
    await AsyncStorage.removeItem('userData');
}