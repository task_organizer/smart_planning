import Frisbee from 'frisbee';
import * as _ from 'lodash';

const baseURI = 'http://localhost:8888';
// const baseURI = 'https://smart-planning.herokuapp.com';

const api = new Frisbee({
    baseURI,
    headers: {
        'Accept':       'application/json',
        'Content-Type': 'application/json'
    }
});

export function getResponseErrorText(response) {

    return _.get(response, 'body.output.payload.message');

}

export function isResponseSuccessful(response) {

    return response.status >= 200 && response.status < 300;

}

export default api;