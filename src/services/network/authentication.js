import api from './api';
import {getResponseErrorText} from './api';


export async function performAuthenticationRequest(email, password) {

    const response = await api.post('/api/login', {
        body: {
            email,
            password
        }
    });

    if (response.status >= 200 && response.status < 300) {

        const user = response.body;
        setApiToken(user);

        return user;

    } else {

        console.log('authentication failed');
        console.log(response.body);
        throw getResponseErrorText(response);

    }

}

export function setApiToken(userData) {

    if (userData && userData.token && userData.token.token) {

        const tokenString = userData.token.token;
        api.jwt(tokenString);

    }

}

export function resetAuthToken() {
    api.jwt(null);
}