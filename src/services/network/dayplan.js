import api, {isResponseSuccessful} from './api';
import {getResponseErrorText} from './api';
import moment from 'moment';
import * as _ from 'lodash';


export async function fetchDayPlan(day) {

    const date = new Date(day);

    const response = await api.get('/api/dayplans', {
        body: {
            $and: [
                {date: {$gte: moment(date).startOf('day').toISOString()}},
                {date: {$lte: moment(date).endOf('day').toISOString()}}
            ]
        }
    });

    if (isResponseSuccessful(response)) {

        if (response.body && response.body.length) {
            return response.body[0];
        }

        return null;

    } else {

        console.log('day plan not found');
        console.log(response.body);
        throw getResponseErrorText(response);

    }

}

export function updateLocallyDayPlanEventStatus(dayPlan, event, isDone) {

    return _.map(dayPlan.events, (dayPlanEvent) => {

        if (dayPlanEvent._id === event._id) {

            return updateEventTaskStatus(dayPlanEvent, isDone);

        } else {

            return dayPlanEvent;

        }

    });

}

export function updateLocallyDayPlanTodoStatus(dayPlan, todo, isDone) {

    return _.map(dayPlan.todos, (dayPlanTodo) => {

        if (dayPlanTodo._id === todo._id) {

            return updateTodoStatus(dayPlanTodo, isDone);

        } else {

            return dayPlanTodo;

        }

    });

}

function updateEventTaskStatus(event, isDone) {

    const updatedTask = {
        ...event.task,
        status: isDone ? 'done' : 'scheduled'
    };

    return {
        ...event,
        task: updatedTask
    }

}

function updateTodoStatus(todo, isDone) {

    return {
        ...todo,
        status: isDone ? 'done' : 'scheduled'
    }

}

export async function updateDayPlanTodoStatus(dayPlan, todo, isDone) {

    const dayPlanId = dayPlan._id;

    const updatedTodo = updateTodoStatus(todo, isDone);

    if (dayPlanId) {

        const response = await api.put(`/api/updateTodoInDayPlan/${dayPlanId}`, {body: updatedTodo});

        if (isResponseSuccessful(response)) {

            const updatedTodos = response.body;
            return updatedTodos;

        } else {

            console.log('todo status cannot be updated');
            console.log(response.body);
            throw getResponseErrorText(response);

        }

    } else {

        console.log('day plan id is required to update the status of todo');

    }

}

export async function updateDayPlanEventStatus(dayPlan, event, isDone) {

    const dayPlanId = dayPlan._id;

    const updatedEvent = updateEventTaskStatus(event, isDone);

    if (dayPlanId) {

        const response = await api.put(`/api/updateEventInDayPlan/${dayPlanId}`, {body: updatedEvent});

        if (isResponseSuccessful(response)) {

            const updatedEvents = response.body;
            return updatedEvents;

        } else {

            console.log('todo status cannot be updated');
            console.log(response.body);
            throw getResponseErrorText(response);

        }

    } else {

        console.log('day plan id is required to update the status of event');

    }

}