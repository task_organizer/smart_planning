export default function (travelMode) {

    switch(travelMode.toLowerCase()) {
        case 'driving':
            return 'car';
        case 'bicycling':
            return 'bicycle';
        case 'transit':
            return 'bus';
        case 'walking':
            return 'male';
        default:
            return 'car';
    }

}