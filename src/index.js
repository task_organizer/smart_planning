import React, {Component} from 'react'
import {createStore, applyMiddleware} from 'redux'
import {Provider, connect} from 'react-redux'
import thunk from 'redux-thunk'
import reducer from './reducers'
import App from './rootViews/App';

const middleware = [thunk];

const store = createStore(
    reducer,
    applyMiddleware(...middleware)
);


export default class Root extends Component {

    render() {

        return <Provider store={store}>
            <App/>
        </Provider>;

    }

}

mapStateToProps = state => { return state };

connect(mapStateToProps)(App);