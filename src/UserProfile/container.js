import {connect} from 'react-redux';
import UserProfileView from './components/UserProfileView';
import {performLogOut} from './../Login/actions';

const mapStateToProps = state => {

    return {};

};

const mapDispatchToProps = (dispatch, ownProps) => ({

    onLogOut: () => {
        dispatch(performLogOut());
    }

});

export default connect(mapStateToProps, mapDispatchToProps)(UserProfileView);