import {combineReducers} from 'redux';

import authentication from './Login/reducers';
import dayPlan from './DayPlan/reducers';
import loading from './rootReducers/loading';
import navigation from './rootReducers/navigation';


const rootReducer = combineReducers({
    navigation,
    authentication,
    loading,
    dayPlan
});

export default rootReducer;
