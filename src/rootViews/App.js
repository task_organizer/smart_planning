import React from 'react'
import PropTypes from 'prop-types';

import Login from '../Login/container';
import DayPlan from '../DayPlan/container';
import NewTask from '../NewTask/container';
import UserProfile from '../UserProfile/container';

import {addNavigationHelpers, StackNavigator, TabNavigator} from 'react-navigation';
import {connect} from 'react-redux';

const AppTabNavigator = TabNavigator({
    DayPlan:     {screen: DayPlan},
    NewTask:     {screen: NewTask},
    UserProfile: {screen: UserProfile}
}, {
    initialRouteName: 'DayPlan'
});


export const AppNavigator = StackNavigator({
    Login: {screen: Login},
    Main:  {screen: AppTabNavigator}
}, {
    mode: 'modal'
});

const AppWithNavigationState = ({dispatch, navigation}) => (
    <AppNavigator navigation={addNavigationHelpers({dispatch, state: navigation})}/>
);

AppWithNavigationState.propTypes = {
    dispatch:   PropTypes.func.isRequired,
    navigation: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    navigation: state.navigation
});

export default connect(mapStateToProps)(AppWithNavigationState);

// export default class Application extends Component {
//
//     render() {
//
//         // let foregroundLoading;
//         //
//         // if (this.state.isPerformingForegroundNetworkRequest) {
//         //     foregroundLoading = <ActivityIndicator animating={true}/>
//         // }
//
//         return <View style={{flex: 1}}>
//             <Auth/>
//             <MainView/>
//         </View>;
//
//     }
//
// }