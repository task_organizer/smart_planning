
export default loading = (state = {}, action) => {

    const isPerformingForegroundNetworkRequest = false;

    return {
        ...state,
        isPerformingForegroundNetworkRequest
    };

};