import { NavigationActions } from 'react-navigation';

import { AppNavigator } from '../rootViews/App';
import {LOGIN_SUCCESSFUL, LOGOUT} from '../Login/actionNames';

const loginAction = AppNavigator.router.getActionForPathAndParams('Login');
const loginNavState = AppNavigator.router.getStateForAction(loginAction);
const initialNavState = AppNavigator.router.getStateForAction(
    loginNavState
);

export default navigation = (state = initialNavState, action) => {
    let nextState;

    switch (action.type) {

        case LOGIN_SUCCESSFUL:
            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate({routeName: 'Main'}),
                state
            );
            break;

        case LOGOUT:

            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate({routeName: 'Login'}),
                state
            );
            break;

        default:
            nextState = AppNavigator.router.getStateForAction(action, state);
            break;

    }

    // Simply return the original `state` if `nextState` is null or undefined.
    return nextState || state;
}