import React, {Component} from 'react';
import {Icon, StyleProvider, getTheme} from 'native-base';
import {Platform} from 'react-native'

const fontAwesome = {
    iconFamily: 'FontAwesome',
    iconFontSize: (Platform.OS === 'ios' ) ? 30 : 28,
    iconMargin: 7,
    iconLineHeight: (Platform.OS === 'ios' ) ? 37 : 30,
};

export class FAIcon extends Component {

    render() {
        return <StyleProvider style={getTheme(fontAwesome)}>
            <Icon name={this.props.name} style={this.props.style}/>
        </StyleProvider>
    }

}