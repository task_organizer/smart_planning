import {connect} from 'react-redux';
import NewTaskView from './components/NewTaskView';
import {performLogOut} from './../Login/actions';

const mapStateToProps = state => {

    return {};

};

const mapDispatchToProps = (dispatch, ownProps) => ({

    onLogOut: () => {
        dispatch(performLogOut());
    }

});

export default connect(mapStateToProps, mapDispatchToProps)(NewTaskView);