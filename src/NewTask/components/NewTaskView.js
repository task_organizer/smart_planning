import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {Content, Form, Item, Input, Button, Icon, Text} from 'native-base';
import {Button as RNButton} from 'react-native';
import {FAIcon} from "../../common/components/fa-icon";

export default class DayPlanView extends Component {

    static propTypes = {
        onLogOut: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {

        this.props.navigation.setParams({
            onLogOut: this.props.onLogOut
        });

    }

    static navigationOptions = ({navigation}) => ({

        headerLeft: <RNButton title='Logout' onPress={() => {navigation.state.params.onLogOut()}}/>,
        tabBarIcon: ({tintColor}) => (
            <FAIcon name='plus'/>
        ),
        tabBarLabel: 'New Task'

    });

    render() {

        return <Text>
            New Task
        </Text>;
    }

}