import {SHOW_LOGIN_ERROR, SHOW_NO_NETWORK_ERROR, LOGIN_SUCCESSFUL, PERFORMING_LOGIN, DISMISS_LOGIN_ERROR, LOGOUT} from './actionNames';
import {performAuthenticationRequest, resetAuthToken, setApiToken} from '../services/network/authentication';
import {showDayPlan} from '../DayPlan/actions';
import {isAuthenticated, getUserData, saveUserData} from '../services/authentication';
import {cleanUserData} from '../services/authentication/index';

export const tryAutoLogin = () => (async (dispatch) => {

    if (await isAuthenticated()) {

        const userModel = await getUserData();
        dispatch(loginSuccessful(userModel));
        setApiToken(userModel);

        dispatch(showDayPlan(Date.now()));

    }

});

export const performLogin = (email, password) => (async (dispatch) => {

    dispatch(showLoginLoading());

    try {

        const userModel = await performAuthenticationRequest(email, password);

        await saveUserData(userModel);

        dispatch(loginSuccessful(userModel));

        dispatch(showDayPlan(Date.now()));

    } catch (error) {

        dispatch(showLoginError(error));

    }

});

export const dismissLoginError = () => {

    return {
        type: DISMISS_LOGIN_ERROR
    };

};

export const performLogOut = () => (async (dispatch) => {

    resetAuthToken();

    await cleanUserData();

    dispatch(showLoginView());

});

const showLoginLoading = () => {

    return {
        type: PERFORMING_LOGIN
    }

};

const loginSuccessful = (userModel) => {

    return {
        type: LOGIN_SUCCESSFUL,
        user: {
            email:    userModel.email,
            fullname: userModel.fullname
        }
    };

};

const showLoginError = (errorText) => {

    return {
        type:       SHOW_LOGIN_ERROR,
        loginError: errorText
    };

};

const showLoginView = () => {

    return {
        type: LOGOUT
    };

};