import {connect} from 'react-redux';
import LoginForm from './components/LoginForm';
import {dismissLoginError, performLogin, tryAutoLogin} from './actions';

const mapStateToProps = state => {

    return {
        isPerformingLogin: state.authentication.isPerformingLogin,
        loginFailed:       state.authentication.loginFailed,
        loginError:        state.authentication.loginError
    }

};

const mapDispatchToProps = (dispatch, ownProps) => ({

    onLogin: (email, password) => {
        dispatch(performLogin(email, password));
    },

    onDismissLoginError: () => {
        dispatch(dismissLoginError());
    },

    tryAutoLogin: () => {
        dispatch(tryAutoLogin());
    }

});

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm)