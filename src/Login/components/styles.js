import {StyleSheet} from 'react-native';

export default styles = {
    appName: {
        marginTop: 130,
        marginBottom: 20,
        textAlign: 'center'
    },
    container: {
        flex:           1,
        padding:        10
    },
    input:     {
        marginTop:  20,
        marginLeft: 0
    },
    button:    {
        marginTop: 20
    },
    loader:    {
        marginLeft: 10
    }
};