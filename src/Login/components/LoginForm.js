import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Content, Form, Item, Input, Button, Text, Icon, H1} from 'native-base';
import {ActivityIndicator, View, Alert} from 'react-native';
import styles from './styles';

export default class LoginForm extends Component {

    static propTypes = {
        onLogin:             PropTypes.func.isRequired,
        onDismissLoginError: PropTypes.func.isRequired,
        isPerformingLogin:   PropTypes.bool,
        loginFailed:         PropTypes.bool,
        loginError:          PropTypes.string
    };

    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            email:               'fedotov4815162342@gmail.com',
            password:            '123qwe'
        };

        this.performLogin = this.performLogin.bind(this);
    }

    componentDidMount() {
        this.props.tryAutoLogin();
    }

    render() {

        let loadingSpinner;

        if (this.props.isPerformingLogin) {
            loadingSpinner = <ActivityIndicator animating={true} style={styles.loader} size="small" color="white"/>;
        }

        if (this.props.loginFailed) {

            Alert.alert(
                'Login Failed',
                this.props.loginError,
                [
                    {
                        text: 'OK', onPress: () => {this.props.onDismissLoginError()}
                    }
                ]
            )

        }

        return <View style={styles.container}>
            <Content>
                <H1 style={styles.appName}>Smart Planning</H1>

                <Form>
                    <Item style={styles.input}>
                        <Icon active name="person"/>
                        <Input placeholder="Email" defaultValue={this.state.email} onChangeText={email => this.setState({email})} autoCapitalize="none"/>
                    </Item>
                    <Item style={styles.input}>
                        <Icon name="unlock"/>
                        <Input placeholder="Password" defaultValue={this.state.password} onChangeText={password => this.setState({password})}/>
                    </Item>

                    <Button style={styles.button} full onPress={this.performLogin}>
                        <Text>Login</Text>
                        {loadingSpinner}
                    </Button>
                </Form>
            </Content>
        </View>

    }

    performLogin() {

        this.props.onLogin(this.state.email, this.state.password);

    }
}