import {PERFORMING_LOGIN, LOGIN_SUCCESSFUL, SHOW_LOGIN_ERROR, DISMISS_LOGIN_ERROR} from './actionNames';

const defaultState = {
    isPerformingLogin: false,
    didLogin:          false,
    loginFailed:       false,
    loginError:        ''
};

export default authentication = (state = defaultState, action) => {

    switch (action.type) {

        case PERFORMING_LOGIN:
            return {
                ...state,
                isPerformingLogin: true,
                loginFailed:       false,
                loginError:        ''
            };

        case LOGIN_SUCCESSFUL:
            return {
                ...state,
                isPerformingLogin: false,
                didLogin:          true,
                loginFailed:       false,
                loginError:        ''
            };

        case SHOW_LOGIN_ERROR:
            return {
                ...state,
                isPerformingLogin: false,
                loginFailed:       true,
                loginError:        action.loginError
            };

        case DISMISS_LOGIN_ERROR:
            return {
                ...state,
                isPerformingLogin: false,
                loginFailed:       false,
                loginError:        ''
            };

    }

    return state;

};