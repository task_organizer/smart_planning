import {connect} from 'react-redux';
import {Linking} from 'react-native';
import DayPlanView from './components/DayPlanView';
import {performLogOut} from './../Login/actions';
import getDestinationLocationAndTravelModeForTravelBuffer from '../services/location/getDestinationLocationForTravelBuffer';
import {
    updateDayPlanEventStatus,
    updateDayPlanTodoStatus,
    updateLocallyDayPlanEventStatus,
    updateLocallyDayPlanTodoStatus
} from '../services/network/dayplan';
import {
    DAY_PLAN_EVENTS_UPDATED,
    DAY_PLAN_OPTIMISTIC_EVENT_STATUS_UPDATE,
    DAY_PLAN_OPTIMISTIC_TODO_STATUS_UPDATE,
    DAY_PLAN_TODOS_UPDATED,
    DAY_PLAN_SELECT_ANOTHER_DAY, DAY_PLAN_ANOTHER_DAY_SELECTION_CANCELED
} from './actionNames';
import {showDayPlan} from './actions';

const mapStateToProps = state => {

    return {
        isPerformFetching: state.dayPlan.isPerformFetching,
        dayPlan: state.dayPlan.dayPlan,
        selectedDate: state.dayPlan.selectedDate,
        isDatePickerVisible: state.dayPlan.isDatePickerVisible
    };

};

const mapDispatchToProps = (dispatch, ownProps) => ({

    onLogOut: () => {
        dispatch(performLogOut());
    },
    onNavigationStart: (travelBuffer) => {

        try {
            const {travelMode, lat, lng} = getDestinationLocationAndTravelModeForTravelBuffer(travelBuffer);

            const queryParameters = `travelmode=${travelMode}&destination=${lat},${lng}`;

            Linking.openURL(`https://www.google.com/maps/dir/?api=1&${queryParameters}`)

        } catch (err) {

            console.error('Navigation is not possible');
            console.error(err);

        }

    },
    onSelectNewDayPlanDate: () => {
        dispatch({
            type: DAY_PLAN_SELECT_ANOTHER_DAY
        });
    },
    onNewDateSelectionCanceled: () => {
        dispatch({
            type: DAY_PLAN_ANOTHER_DAY_SELECTION_CANCELED
        });
    },
    onNewDayPlanDateSelected: (newDate) => {
        dispatch(showDayPlan(newDate));
    },
    onRefresh: (forDate) => {
        dispatch(showDayPlan(forDate));
    },
    async changeEventTaskStatus(dayPlan, event, isDone) {

        dispatch({
            type: DAY_PLAN_OPTIMISTIC_EVENT_STATUS_UPDATE,
            updatedEvents: updateLocallyDayPlanEventStatus(dayPlan, event, isDone)
        });

        const updatedEvents = await updateDayPlanEventStatus(dayPlan, event, isDone);

        dispatch({
            type: DAY_PLAN_EVENTS_UPDATED,
            updatedEvents
        });

    },
    async changeTodoTaskStatus(dayPlan, todo, isDone) {

        dispatch({
            type: DAY_PLAN_OPTIMISTIC_TODO_STATUS_UPDATE,
            updatedTodos: updateLocallyDayPlanTodoStatus(dayPlan, todo, isDone)
        });

        const updatedTodos = await updateDayPlanTodoStatus(dayPlan, todo, isDone);

        dispatch({
            type: DAY_PLAN_TODOS_UPDATED,
            updatedTodos
        });

    }

});

export default connect(mapStateToProps, mapDispatchToProps)(DayPlanView);