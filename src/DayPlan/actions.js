import {DAY_PLAN_FETCHED, FETCHING_DAY_PLAN} from './actionNames';
import {fetchDayPlan} from '../services/network/dayplan';

export const showDayPlan = (day) => (async (dispatch) => {

    day = new Date(day);

    dispatch({
        type: FETCHING_DAY_PLAN,
        selectedDate: day
    });

    const dayPlan = await fetchDayPlan(day);

    if(dayPlan) {

        dispatch({
            type: DAY_PLAN_FETCHED,
            dayPlan,
            selectedDate: day
        });

    } else {

        dispatch({
            type: DAY_PLAN_FETCHED,
            dayPlan: null,
            selectedDate: day
        });

    }

});


const shouldFetchDayPlan = (state) => {

    if (state.lastUpdateTime) {

    }

};