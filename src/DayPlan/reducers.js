import {
    DAY_PLAN_ANOTHER_DAY_SELECTION_CANCELED,
    DAY_PLAN_EVENTS_UPDATED,
    DAY_PLAN_FETCHED, DAY_PLAN_OPTIMISTIC_EVENT_STATUS_UPDATE, DAY_PLAN_OPTIMISTIC_TODO_STATUS_UPDATE,
    DAY_PLAN_SELECT_ANOTHER_DAY,
    DAY_PLAN_TODOS_UPDATED,
    FETCHING_DAY_PLAN
} from './actionNames';

const defaultState = {
    dayPlan: {
        events: []
    },
    selectedDate: new Date(),
    isPerformFetching: false
};

export default dayPlan = (state = defaultState, action) => {

    switch (action.type) {

        case DAY_PLAN_FETCHED:
            return {
                ...state,
                isPerformFetching: false,
                dayPlan: action.dayPlan,
                selectedDate: action.selectedDate
            };

        case FETCHING_DAY_PLAN:
            return {
                ...state,
                isPerformFetching: true,
                isDatePickerVisible: false,
                selectedDate: action.selectedDate
            };

        case DAY_PLAN_OPTIMISTIC_TODO_STATUS_UPDATE:
        case DAY_PLAN_TODOS_UPDATED: {

            const updatedTodos = action.updatedTodos;

            const updatedDayPlan = {
                ...state.dayPlan,
                todos: updatedTodos
            };

            return {
                ...state,
                dayPlan: updatedDayPlan
            };
        }

        case DAY_PLAN_EVENTS_UPDATED:
        case DAY_PLAN_OPTIMISTIC_EVENT_STATUS_UPDATE: {
            const updatedEvents = action.updatedEvents;
            const updatedDayPlan = {
                ...state.dayPlan,
                events: updatedEvents
            };

            return {
                ...state,
                dayPlan: updatedDayPlan
            };
        }

        case DAY_PLAN_SELECT_ANOTHER_DAY: {
            return {
                ...state,
                isDatePickerVisible: true
            }
        }

        case DAY_PLAN_ANOTHER_DAY_SELECTION_CANCELED: {
            return {
                ...state,
                isDatePickerVisible: false
            }
        }

    }

    return state;

};