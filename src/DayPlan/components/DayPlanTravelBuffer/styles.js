const dirtyWhite = 'rgb(235,235,235)';

export default styles = {
    mainContainer: {
        marginLeft: 10,
        flex: 1
    },
    travelBufferCard: {
        backgroundColor: dirtyWhite
    },
    infoContainer: {
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: dirtyWhite,
        borderBottomColor: 'transparent'
    },
    titleContainer: {
        flexDirection: 'row',
        alignItems: 'stretch'
    },
    footer: {
        justifyContent: 'space-between',
        backgroundColor: dirtyWhite,
        borderTopColor: 'transparent'
    },

    title: {
        fontSize: 20,
        marginLeft: 0
    },
    travelModeIconContainer: {
        height: 20,
        width: 40
    },
    travelModeIcon: {
        fontSize: 24
    }
};