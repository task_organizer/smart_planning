import React, {Component} from 'react';
import PropTypes from 'prop-types'
import styles from './styles';
import moment from 'moment';
import {Button, Card, CardItem, Text, View} from 'native-base';
import getTravelModeIconName from '../../../services/formatters/getTravelModeIconName';
import {FAIcon} from '../../../common/components/fa-icon';

export default class DayPlanTravelBuffer extends Component {

    static propTypes = {
        travelBuffer: PropTypes.object.isRequired,
        onNavigationStart: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
    }

    getTravelBufferDurationText(travelBuffer) {
        const duration = _.first(travelBuffer.title.match(/\(.+?\)/));
        return duration.replace(/\(|\)/g, '');
    }

    getTravelBufferTitleInfo(travelBuffer) {

        const duration = this.getTravelBufferDurationText(travelBuffer);
        return <View style={styles.titleContainer}>

            <Text style={styles.title}>
                <View style={styles.travelModeIconContainer}>
                    <FAIcon style={styles.travelModeIcon} name={getTravelModeIconName(travelBuffer.travelMode)}/>
                </View>
                {duration}
            </Text>

        </View>;

    }

    render() {

        const travelBuffer = this.props.travelBuffer;

        const title = this.getTravelBufferTitleInfo(travelBuffer);

        return <View style={styles.mainContainer}>
            <Card style={styles.travelBufferCard}>

                <CardItem style={{...styles.infoContainer}}>

                    {title}

                </CardItem>

                <CardItem footer style={styles.footer}>

                    <Text></Text>
                    <Button transparent onPress={() => {
                        this.props.onNavigationStart(travelBuffer)
                    }}>
                        <FAIcon style={styles.travelModeIcon} name={'location-arrow'}/>
                        <Text>Start Navigation</Text>
                    </Button>

                </CardItem>
            </Card>
        </View>;

    }

}