export default styles = {
    mainContainer: {
        marginLeft: 10,
        flex: 1
    },
    infoContainer: {
        flexDirection: 'column',
        alignItems: 'stretch',
        marginBottom: 20
    },
    titleContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    addressContainer: {
        marginTop: 10,
        marginLeft: 2
    },
    footer: {
        justifyContent: 'space-between'
    },

    doneCheckBox: {
        marginLeft: -10
    },
    title: {
        marginLeft: 20,
        fontSize: 22,
    },
    address: {
        fontSize: 10
    },
    locationIconContainer: {
        height: 10,
        width: 12
    },
    categoryIconContainer: {
        height: 14,
        marginTop: 3,
        width: 18
    },
    categoryIcon: {
        fontSize: 12
    }
};