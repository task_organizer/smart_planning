import React, {Component} from 'react';
import PropTypes from 'prop-types'
import styles from './styles';
import {FAIcon} from "../../../common/components/fa-icon";
import moment from "moment";
import {Button, Card, CardItem, CheckBox, Text, View} from 'native-base';
import {isDayPlanEventDone} from '../../../services/task/isTaskStatusDone';
import {TouchableOpacity} from 'react-native';

export default class DayPlanEvent extends Component {

    static propTypes = {
        event: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
    }

    getEventItemSpacingSize(start, end) {

        const momentStart = moment(start);
        const momentEnd = moment(end);

        const hourSpacing = 2;

        const duration = moment.duration(momentEnd - momentStart);
        const hours = duration.asHours();

        return hourSpacing * hours;

    }

    getEventAddressInfo(event) {

        if(event && event.location && event.location.address) {

            return <View style={styles.addressContainer}>
                <Text style={styles.address}>
                    <View style={styles.locationIconContainer}>
                        <FAIcon style={styles.categoryIcon} name={'map-marker'}/>
                    </View>
                    {event.location.address}
                </Text>
            </View>;

        }

        return;

    }

    getEventTitleInfo(event) {

        const isTaskDone = isDayPlanEventDone(event);
        const updateDoneStatus = () => this.props.changeTaskStatus(event, !isTaskDone);

        return <TouchableOpacity style={styles.titleContainer} onPress={updateDoneStatus}>

            <CheckBox style={styles.doneCheckBox} checked={isTaskDone} onPress={updateDoneStatus}/>

            <Text style={styles.title} numberOfLines={1}>
                {event.title}
            </Text>

        </TouchableOpacity>;

    }

    getEventCategoryInfo(event) {

        if(event && event.task && event.task.category) {

            return <Text note>
                <View style={styles.categoryIconContainer}>
                    <FAIcon style={styles.categoryIcon} name={'tag'}/>
                </View>
                {event.task.category}
            </Text>

        } else {

            return <Text></Text>

        }

    }

    render() {

        const event = this.props.event;
        const eventSpacing = this.getEventItemSpacingSize(event.start, event.end);

        const eventTitle = this.getEventTitleInfo(event);
        const eventAddress = this.getEventAddressInfo(event);
        const eventCategory = this.getEventCategoryInfo(event);

        return <View style={styles.mainContainer}>
            <Card>

                <CardItem style={{...styles.infoContainer, marginBottom: eventSpacing}}>

                    {eventTitle}
                    {eventAddress}

                </CardItem>

                <CardItem footer style={styles.footer}>

                    {eventCategory}

                    <Button transparent>
                        <Text>More...</Text>
                    </Button>

                </CardItem>
            </Card>
        </View>;

    }

}