import React, {Component} from 'react';
import PropTypes from 'prop-types'
import styles from './styles';
import {Card, CardItem, CheckBox, Text, View} from 'native-base';
import {isTodoDone} from '../../../services/task/isTaskStatusDone';
import {TouchableOpacity} from 'react-native';

export default class ToDoList extends Component {

    static propTypes = {
        todos: PropTypes.array.isRequired,
        changeTaskStatus: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
    }

    renderToDoItem(todo) {

        const isTaskDone = isTodoDone(todo);
        const updateDoneStatus = () => this.props.changeTaskStatus(todo, !isTaskDone);

        return <Card style={styles.cardContainer} key={todo._id}>
            <CardItem>

                <TouchableOpacity onPress={updateDoneStatus} style={styles.todoButton}>
                    <CheckBox style={styles.doneCheckBox} checked={isTaskDone}
                              onPress={updateDoneStatus}/>

                    <Text style={styles.title}>
                        {todo.title}
                    </Text>
                </TouchableOpacity>

            </CardItem>
        </Card>

    }

    render() {

        const todoItems = this.props.todos.map(this.renderToDoItem.bind(this));

        return <View style={styles.mainContainer}>{todoItems}</View>

    }

}