export default styles = {
    mainContainer: {
        margin: 20,
        marginTop: 10,
        marginBottom: 0
    },
    cardContainer: {
        margin: 20,
        flex: -1
    },
    todoItemsContainer: {
        flexDirection: 'column',
        alignItems: 'flex-start'
    },
    todoContainer: {
        margin: 10
    },
    todoButton: {
        flexDirection: 'row'
    },
    doneCheckBox: {
        marginLeft: -10
    },
    title: {
        marginLeft: 20
    },
};