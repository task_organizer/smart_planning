export default styles = {
    mainContainer: {
        flex: 1
    },
    selectedDate: {
        marginTop: 10,
        textAlign: 'center'
    },
    noDayPlan: {
        marginTop: 30,
        textAlign: 'center'
    }
}