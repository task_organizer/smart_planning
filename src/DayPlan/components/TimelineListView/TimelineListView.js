import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {ListView} from 'react-native';
import styles from './styles';
import {Text, View} from 'native-base';
import {formatTime} from '../../../services/formatters/timeFormatters';

export default class TimelineListView extends Component {

    static propTypes = {
        events: PropTypes.array.isRequired,
        eventRenderer: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        const ds = new ListView.DataSource({rowHasChanged: this.listViewRowHasChanged});
        this.state = {
            dataSource: ds,
            events:     ds.cloneWithRows(props.events),
        };
    }

    componentWillReceiveProps(props) {

        const dataSource = this.state.dataSource;

        this.setState({
            events: dataSource.cloneWithRows(props.events),
        });

    }

    listViewRowHasChanged(r1, r2,) {
        return r1 !== r2;
    }

    renderTimeLineRow(event) {

        const eventStart = formatTime(event.start);
        const eventEnd = formatTime(event.end);

        return <View style={styles.taskItem}>

            <View style={styles.taskTimeContainer}>
                <Text>{eventStart}</Text>
                <View style={styles.taskTimeConnector}/>
                <Text>{eventEnd}</Text>
            </View>

            {this.props.eventRenderer(event)}

        </View>;

    }

    render() {

        return <ListView
            enableEmptySections={true}
            style={styles.timelineListView}
            dataSource={this.state.events}
            renderRow={this.renderTimeLineRow.bind(this)}
        />;

    }

}