
export default styles = {
    timelineListView: {
        flex: 1,
        margin: 20
    },
    taskItem: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 10
    },
    taskTimeContainer: {
        minWidth: 45,
        paddingTop: 10,
        paddingBottom: 10,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    taskStartTime: {
        fontSize: 14,
        color: 'grey'
    },
    taskEndTime: {
        fontSize: 14,
        color: 'grey'
    },
    taskTimeConnector: {
        alignSelf: 'center',
        borderLeftWidth: 2,
        borderColor: 'lightgrey',
        flex: 1,
        width: 1
    }
};