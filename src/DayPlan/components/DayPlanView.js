import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {Button as RNButton, RefreshControl, ScrollView, View} from 'react-native';
import {FAIcon} from '../../common/components/fa-icon';
import TimelineListView from './TimelineListView/TimelineListView';
import styles from './styles';
import ToDoList from './ToDoList/ToDoList';
import * as _ from 'lodash';
import DayPlanEvent from './DayPlanEvent/DayPlanEvent';
import DayPlanTravelBuffer from './DayPlanTravelBuffer/DayPlanTravelBuffer';
import moment from 'moment';
import {H2, H3} from 'native-base';
import {formatDay} from '../../services/formatters/dayFormatter';
import DateTimePicker from 'react-native-modal-datetime-picker';

export default class DayPlanView extends Component {

    static propTypes = {

        onLogOut: PropTypes.func.isRequired,
        onNavigationStart: PropTypes.func.isRequired,
        isPerformFetching: PropTypes.bool,

        dayPlan: PropTypes.shape({
            events: PropTypes.array,
            travelBuffers: PropTypes.array,
            todos: PropTypes.array
        }),
        changeTodoTaskStatus: PropTypes.func.isRequired,
        changeEventTaskStatus: PropTypes.func.isRequired,
        selectedDate: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object
        ]).isRequired,

        isDatePickerVisible: PropTypes.bool,
        onNewDayPlanDateSelected: PropTypes.func.isRequired,
        onNewDateSelectionCanceled: PropTypes.func.isRequired,
        onSelectNewDayPlanDate: PropTypes.func.isRequired,
        onRefresh: PropTypes.func.isRequired
    };

    componentDidMount() {

        this.props.navigation.setParams({
            onLogOut: this.props.onLogOut,
            onSelectNewDayPlanDate: this.props.onSelectNewDayPlanDate
        });

    }

    static navigationOptions = ({navigation}) => {

        return {

            headerLeft: <RNButton title='Logout' onPress={() => {
                navigation.state.params.onLogOut()
            }}/>,
            headerRight: <RNButton title='Change Day' onPress={() => {
                navigation.state.params.onSelectNewDayPlanDate()
            }}/>,
            tabBarIcon: ({tintColor}) => (
                <FAIcon name='calendar'/>
            ),
            tabBarLabel: 'Day Plan'

        };

    };

    getToDoList(dayPlan) {

        if (dayPlan && dayPlan.todos && dayPlan.todos.length) {
            return <ToDoList todos={dayPlan.todos}
                             changeTaskStatus={this.props.changeTodoTaskStatus.bind({}, dayPlan)}/>;
        }

    }

    dayPlanItemRenderer(dayPlan, dayPlanItem) {

        if (dayPlanItem.task) {

            return <DayPlanEvent event={dayPlanItem}
                                 changeTaskStatus={this.props.changeEventTaskStatus.bind({}, dayPlan)}/>

        } else if (dayPlanItem.directions) {

            return <DayPlanTravelBuffer travelBuffer={dayPlanItem} onNavigationStart={this.props.onNavigationStart}/>

        }

    }

    getTimeline(dayPlan) {
        const events = dayPlan && dayPlan.events;

        if (events && events.length) {

            const orderedDayPlanItems = _.sortBy(dayPlan.events.concat(dayPlan.travelBuffers || []), (dayPlanItem) => {
                return moment(dayPlanItem.start);
            });

            return <TimelineListView events={orderedDayPlanItems}
                                     eventRenderer={this.dayPlanItemRenderer.bind(this, dayPlan)}/>;

        }
    }

    getScreenContent(dayPlan) {
        const todoList = this.getToDoList(dayPlan);
        const timeLine = this.getTimeline(dayPlan);

        return (!todoList && !timeLine) ?
            <H3 style={styles.noDayPlan}>No day plan created</H3> :
            <View>{todoList}{timeLine}</View>;
    }

    getDatePicker(isDatePickerVisible, date, onNewDateSelected, onNewDateSelectionCanceled) {
        return <DateTimePicker
            date={date}
            onConfirm={onNewDateSelected}
            onCancel={onNewDateSelectionCanceled}
            mode='date'
            isVisible={isDatePickerVisible}/>
    }

    getRefreshControl(onRefresh, isRefreshing) {
        return <RefreshControl
            refreshing={isRefreshing}
            onRefresh={onRefresh}
        />
    }

    render() {

        const day = formatDay(this.props.selectedDate);
        const pageHeading = <H2 style={styles.selectedDate}>{day}</H2>;

        const screenContent = this.getScreenContent(this.props.dayPlan);

        const datePicker = <View>{this.getDatePicker(this.props.isDatePickerVisible, this.props.selectedDate, this.props.onNewDayPlanDateSelected, this.props.onNewDateSelectionCanceled)}</View>;

        const refreshControl = this.getRefreshControl(this.props.onRefresh.bind({}, this.props.selectedDate), this.props.isPerformFetching);

        return <ScrollView style={styles.mainContainer} refreshControl={refreshControl}>

            {pageHeading}

            {screenContent}

            {datePicker}

        </ScrollView>

    }

}